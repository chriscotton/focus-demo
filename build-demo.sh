#!/bin/bash 
set -x 
for i in `cat demo.txt` 
do   
	    meteor-boilerplate create:test $i  
	    meteor-boilerplate create:view $i 
	    meteor-boilerplate create:layout $i 
	    meteor-boilerplate create:route $i 
	    meteor-boilerplate create:model $i 
	    meteor-boilerplate create:module $i 
done

meteor-boilerplate create:common main_layout

