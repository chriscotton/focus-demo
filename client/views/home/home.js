 if (Meteor.isClient) {
  var audio
  var count
  var timer
  var gender
  var lang
  var voice
  var currentSlide = 1
  var markers = {}
  var wordList_widget
  var word_widgets = []
  var ispeech_api_key = '743dc21567e994cb86846dc18222313e'
  var ispeech_api_key_dev = '59e482ac28dd52db23a22aff4ac1d31e'
  var ispeech_tts = 'http://api.ispeech.org/api/rest?format=mp3&action=convert&apikey='
  var ispeech_tts_marker = 'http://api.ispeech.org/api/rest?action=markers&apikey='
  
  function getMakers(msg, callback){
    var url = ispeech_tts_marker + ispeech_api_key + '&voice='+ msg.voice + '&text=' + msg.toSay;    
     $.ajax({
        type: "GET",
        url: url,
        cache: false,
        dataType: "xml",
        success: function(xml) {
            console.log('success')
            markers.text = $(xml).find('text').eq(0).text().trim()
            markers.length = parseInt($(xml).find('length').eq(0).text())
            markers.count = parseInt($(xml).find('words').text())
            markers.voice = $(xml).find('voice').text()
            markers.words = []
            wordList_widget.html("");
            $(xml).find('word').each(function(index){
              var word = {
                start: parseInt($(this).find('start').text()),
                end: parseInt($(this).find('end').text()),
                index: parseInt($(this).find('index').text()),
                length: parseInt($(this).find('length').text()),
                text: $(this).find('text').text()
              }
              markers.words.push(word)
              word_widgets[index] = $("<span>").text(word.text);
              wordList_widget.append(word_widgets[index]);
              if(markers.voice == "usenglishmale" || markers.voice == "usenglishfemale"){
                wordList_widget.append(" ")
              }
            })
            callback()
        }
    });
  }

  function speak(msg){
    if(msg.voice == voice && audio != undefined){
        var url = ispeech_tts + ispeech_api_key_dev + '&voice=' + msg.voice + '&text=' + msg.toSay;
        audio.src = url;
        if(markers.voice == 'hkchinesefemale'){
            console.log('hk')
            wordList_widget.html("");
            words = markers.text.split("")
            for(var i = 0; i < words.length; i++){
              word_widgets[i] = $("<span>").text(words[i]);
              wordList_widget.append(word_widgets[i]);
              wordList_widget.append(" ")
            }
            var i = 0
            audio.addEventListener("timeupdate", function(){
              if(audio.currentTime !=0 && i < words.length){
                console.log(audio.currentTime)
                $('i.fa-spin').fadeOut()
                wordList_widget.find('span').removeClass('highlight')
                word_widgets[i].addClass("highlight");
                i++  
              }
          })
      }else{
        audio.addEventListener("timeupdate", function(){
          if(audio.currentTime !=0){
            $('i.fa-spin').fadeOut()
          }
          var time = audio.currentTime * 1000
          var words =  markers.words
          for(var i = 0; i < markers.count; i++){
            if(time > words[i].start && time < words[i].end){
              wordList_widget.find('span').removeClass('highlight')
              word_widgets[i].addClass("highlight");
              break;  
            }
          }
        })
      }
      audio.addEventListener("ended", function(){
        wordList_widget.find('span').removeClass('highlight')
      })
      audio.play();  
    }else{
        getMakers(msg, function(){
          var url = ispeech_tts + ispeech_api_key_dev + '&voice=' + msg.voice + '&text=' + msg.toSay;
          audio = new Audio();    
          audio.src = url;
          if(markers.voice == 'hkchinesefemale'){
            console.log('hk')
            wordList_widget.html("");
            words = markers.text.split("")
            for(var i = 0; i < words.length; i++){
              word_widgets[i] = $("<span>").text(words[i]);
              wordList_widget.append(word_widgets[i]);
              wordList_widget.append(" ")
            }
            var i = 0
            audio.addEventListener("timeupdate", function(){
              if(audio.currentTime !=0 && i < words.length){
                console.log(audio.currentTime)
                $('i.fa-spin').fadeOut()
                wordList_widget.find('span').removeClass('highlight')
                word_widgets[i].addClass("highlight");
                i++  
              }
          })
          }else{
            audio.addEventListener("timeupdate", function(){
              if(audio.currentTime !=0){
                $('i.fa-spin').fadeOut()
              }
              var time = audio.currentTime * 1000
              var words =  markers.words
              for(var i = 0; i < markers.count; i++){
                if(time > words[i].start && time < words[i].end){
                  wordList_widget.find('span').removeClass('highlight')
                  word_widgets[i].addClass("highlight");
                  break;  
                }
              }
            })
          }
          audio.addEventListener("ended", function(){
            wordList_widget.find('span').removeClass('highlight')
          })
          audio.play();  
        })
    }
    voice = msg.voice
  }

Template.home.helpers({
    'feature' : function () {
        return [
            { 'text' : 'Games', 'icon' : 'fa fa-gamepad', 'color' : 'hover-orange', 'path' : '/games' },
            { 'text' : 'Google Map', 'icon' : 'fa fa-map-marker', 'color' : 'hover-orange', 'path' : '/google-maps' },
            { 'text' : 'Text To Speech', 'icon' : 'fa fa-microphone', 'color' : 'hover-orange', 'path' : '/tts' },
            { 'text' : 'Chat', 'icon' : 'fa fa-comments', 'color' : 'hover-orange', 'path' : '/chat' }
        ]
    },
    'package' : function () {
        return [
            { 'name' : 'Iron Router', 'path' : 'https://github.com/EventedMind/iron-router' },
            { 'name' : 'Collection2', 'path' : 'https://github.com/aldeed/meteor-collection2' },
            { 'name' : 'Semantic UI', 'path' : 'http://semantic-ui.com/' },
            { 'name' : 'less', 'path' : 'http://lesscss.org/' },
            { 'name' : 'jQuery', 'path' : 'http://jquery.com/' },
            { 'name' : 'Underscore', 'path' : 'http://underscorejs.org/' },
            { 'name' : 'Handlebar Helpers', 'path' : 'https://github.com/raix/Meteor-handlebar-helpers' },
            { 'name' : 'Iron Router Progress', 'path' : 'https://github.com/Multiply/iron-router-progress' },
            { 'name' : 'Accounts UI & Password', 'path' : 'http://docs.meteor.com/#accountsui' }
        ];
    },
    'consoleCommand' : function () {
        return [
            { 'command' : 'create:view', 'description' : 'Creates a folder under client/views with html, less and javascript files.' },
            { 'command' : 'create:module', 'description' : 'Similiar to a view, but under client/modules and for re-usable components' },
            { 'command' : 'create:layout', 'description' : 'Creates a layout template which yields your content, used by iron-router' },
            { 'command' : 'create:common', 'description' : 'Creates a simple html file under client/views/common' },
            { 'command' : 'create:route', 'description' : 'Creates a route javascript file under client/routes' },
            { 'command' : 'create:model', 'description' : 'Creates a model with files in model/, client/subscriptions and server/publications' },
            { 'command' : 'create:test', 'description' : 'Creates test file under tests' }
        ];
    },
    'semanticElement' : function () {
        return [
            { 'what' : 'Large Buttons', 'withBootstrap' : 'btn btn-lg', 'withSemanticUI' : 'ui large button' },
            { 'what' : 'One column', 'withBootstrap' : 'col-md-1', 'withSemanticUI' : 'one wide column' },
            { 'what' : 'Vertical Menu / Navigation', 'withBootstrap' : 'nav nav-pills', 'withSemanticUI' : 'ui vertical menu' }

        ];
    },
    'bootstrapCode' : function () {
        return '<div class="btn btn-primary btn-lg"></div>';
    },
    'folder' : function () {
        return [
            { 'root' : 'client', 'children' :
                ['compatibility', 'config', ' lib', ' routes', ' startup', ' stylesheets', 'subscriptions',
                 'modules', 'views']
            },
            { 'root' : 'model' },
            { 'root' : 'private' },
            { 'root' : 'server', 'children' : ['fixtures', 'lib', 'publications', 'startup', 'tests'] },
            { 'root' : 'public' },
            { 'root' : 'meteor-boilerplate' },
            { 'root' : 'meteor-boilerplate.bat' }
        ]
    }
});

Template.home.events({
    'click .english': function(){
        wordList_widget = $("#wordList");
        {
            wordList_widget.removeClass('chinese')
        }
        wordList_widget.text('A koala baby sits on it’s mother')
        gender = 'female'
        lang = "usenglish"
    },
    'click .cantonese': function(){
        wordList_widget = $("#wordList");
        if(wordList_widget.hasClass('chinese'))
        {
            wordList_widget.removeClass('chinese')
        }
        wordList_widget.addClass('chinese')
        wordList_widget.text('一只考拉宝宝坐在妈妈的身上')
        gender = 'female'
        lang = "chchinese"
    },
    'click .w-nav': function(){
        $('.text-block i.btn-speak').fadeIn()
        image = "<img src='/images/koala.jpg'></img>"
        $('.slide1').html(image)
    },
    'click .btn-speak': function(){
        $('i.fa-spin').show()
        wordList_widget = $("#wordList");
        var toSay = wordList_widget.text().trim()
        // var toSay = $('#content :selected').text().trim()
        // gender = $('input[name=gender]:checked').val()
        // lang = $('#lang :selected').val();
        // gender = 'male'
        // lang = "usenglish"
        speak({toSay: toSay, voice: lang+gender})
    },
    'click .w-slider-arrow-left': function(){
        if (currentSlide > 1){
            currentSlide = currentSlide - 1
            $('.w-slide').hide()
            $('.slide'+currentSlide).fadeIn()
        }
    },
    'click .w-slider-arrow-right': function(){
        if (currentSlide < 3){
            currentSlide = currentSlide + 1
            $('.w-slide').hide()
            $('.slide'+currentSlide).fadeIn()
        }
    },
    'click .w-slider-nav-dot': function(e){
        currentSlide = $(e.target).data('index')
        $('.w-slide').hide()
        $('.slide'+currentSlide).fadeIn()
        $('.w-slider-nav-dot').removeClass('active')
        $('.dot'+currentSlide).addClass('active')
    }
});


Template.home.rendered = function () {
    // @see: http://stackoverflow.com/questions/5284814/jquery-scroll-to-div
    gender = 'female'
    lang = "usenglish"
    $('a[href*=#]:not([href=#])').click(function () {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }

        return true;
    });
};
}
