
/**
 * maze2.js
 *
 * Kids Dailies Maze JS.
 *
 * @author Adrian Iacob <adrian@bmcd.co>
 * @author Nitin N <nitin@bmcd.co>
 * @author Brent McDowell <brent@bmcd.co>
 * @author Bruce Huang <bruce@bmcd.co>
 * @version 0.1
 * @package games
 */

 // this should come from a game collection..
var img_data = { "data" : { "rad_array" : [ 30,
          30,
          30,
        ],
      "seconds" : 120,
      "x_array" : [ 445,
          504,
          621,
     
        ],
      "y_array" : [ 460,
          412,
          400,
        ],
      "right_choice": 1,
      "image_path" : "/images/games/maze2.jpg", //can be internet available image path
    }
};

var total_seconds = img_data.data.seconds + 1;
var x_array = img_data.data.x_array;     
var y_array = img_data.data.y_array;
var rad_array =img_data.data.rad_array;
var right_choice_index = img_data.data.right_choice || 0;
var image_path = img_data.data.image_path || "/images/games/maze2.jpg";

if (Meteor.isClient) {
  var canvas;
  var context;
  var count = 0;

  window.onload = function() {
    

  };
  function Cirkus(centerX,centerY, radius, color ) {

        context.beginPath();
        context.arc(centerX, centerY, radius, 0, 2*Math.PI, false);
        context.lineWidth = 2;
        context.strokeStyle = color;
        context.stroke();
    }

  position = function(e) {
    if(count > y_array.length - 1)
    {
      return false;
    }
    var x_diff;
    var y_diff;
    var distance0, distance1;
    var width=document.getElementById('canvas').width;
    var offset_left =  document.getElementById('canvas').offsetLeft;
    var offset_top =  document.getElementById('canvas').offsetTop;

    e = arguments.callee.caller.arguments[0] || window.event

    if (e.pageX == null && e.clientX != null ) { 
      var html = document.documentElement
      var body = document.body
  
      e.pageX = e.clientX + (html && html.scrollLeft || body && body.scrollLeft || 0) - (html.clientLeft || 0)
      e.pageY = e.clientY + (html && html.scrollTop || body && body.scrollTop || 0) - (html.clientTop || 0)
    }
    x=e.pageX;
    y=e.pageY;
    x = x - offset_left;
    y = y - offset_top;

    if(x_array.length == y_array.length) {
      length = x_array.length;
      for (var i = 0; i < length; i++) {
        x_diff=x_array[i]-x;
        y_diff=y_array[i]-y;
        x_diff1=x_diff+width/2;
        distance0=Math.sqrt(x_diff*x_diff+y_diff*y_diff);
        distance1=Math.sqrt(x_diff1*x_diff1+y_diff*y_diff);
        
        if ( (distance0 < rad_array[i])||(distance1 < rad_array[i]))  {
          count++;
          if(i === right_choice_index)
          { 
            Cirkus(x_array[i],y_array[i],rad_array[i],'green');
            $('#winModal').modal()
          }
          else
          {
            Cirkus(x_array[i],y_array[i],rad_array[i],'red');
            $('#loseModal').modal()
          }
          //here may add code to load next image
        }
      }
    }
  }


  function drawMaze(mazeFile, startingX, startingY) {


    // Load the maze picture.
    var imgMaze = new Image();
    imgMaze.onload = function() {
      // Resize the canvas to match the maze picture.
      canvas.width = imgMaze.width;
      canvas.height = imgMaze.height;

      // Draw the maze.
      context.drawImage(imgMaze, 0,0);

    };
    imgMaze.src = mazeFile;
  }

  

  Template.GameCanvasMaze2.rendered = function () {
  	// Set up the canvas.
    canvas = document.getElementById("canvas");
    context = canvas.getContext("2d");
    count = 0;
    // Draw the maze background.
    drawMaze(image_path, 268, 5);
  };

  Template.GameCanvasMaze2.events({
    'click #canvas': function(evt) {
        position(evt);
    },
    'click .tryagain': function(evt){
      window.location.href = '/games/maze-2'
    }
  });

}

if (Meteor.isServer) {
  Meteor.startup(function () {
    // code to run on server at startup
  });
}
