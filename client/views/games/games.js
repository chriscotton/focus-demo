Template.GamesTemplate.helpers({ 
	'games' : function() {
		return [
            { 'name' : 'Crossword', 'path' : '/games/crossword' },
            { 'name' : 'Image Scamble', 'path' : '/games/image-scramble' },
            { 'name' : 'Maze', 'path' : '/games/maze' },
            { 'name' : 'Maze2', 'path' : '/games/maze-2' },
            { 'name' : 'Spot the different', 'path' : '/games/spot-the-difference' },
            { 'name' : 'Tic tac toe', 'path' : '/games/tic-tac-toe' },
            { 'name' : 'Word Search', 'path' : '/games/wordsearch' }
        ]
	}
})