
/**
 * spot-the-difference.js
 *
 * Kids Dailies Spot the Difference JS.
 *
 * @author Bruce Huang <bruce@bmcd.co>
 * @version 0.1
 * @package games
 */

// this should come from a game collection..
var img_data = { "data" : { "rad_array" : [ 16,
          23,
          16,
          20,
          116,
        ],
      "seconds" : 120,
      "x_array" : [ 276,
          264,
          330,
          78,
          122,
        ],
      "y_array" : [ 178,
          124,
          517,
          321,
          533,
        ],
      "img_width": 674,
      "img_height": 708,
    }
};

var total_seconds = img_data.data.seconds + 1;
var x_array = img_data.data.x_array;     
var y_array = img_data.data.y_array;
var rad_array =img_data.data.rad_array;
var img_width = img_data.data.img_width || 500;
var img_height =img_data.data.img_height || 500;

if (Meteor.isClient) {
  var x,y;
  var count = 0;
  var length = 0;

  function cirkus(centerX,centerY, radius ) {
    var canvas=document.getElementById("Canvas");
    var img=document.getElementById('image1'); 

    var obCanvas = canvas.getContext('2d');
   
    obCanvas.beginPath();
    obCanvas.arc(centerX, centerY, radius, 0, 2*Math.PI, false);
    obCanvas.lineWidth = 2;
    obCanvas.strokeStyle = 'blue';
    obCanvas.stroke();
  }

  function position(e) {
    var x_diff;
    var y_diff;
    var distance0, distance1;
    var width=document.getElementById('Canvas').width;
    var offset_left =  document.getElementById('Canvas').offsetLeft;
    var offset_top =  document.getElementById('Canvas').offsetTop;

    e = arguments.callee.caller.arguments[0] || window.event

    if (e.pageX == null && e.clientX != null ) { 
      var html = document.documentElement
      var body = document.body
  
      e.pageX = e.clientX + (html && html.scrollLeft || body && body.scrollLeft || 0) - (html.clientLeft || 0)
      e.pageY = e.clientY + (html && html.scrollTop || body && body.scrollTop || 0) - (html.clientTop || 0)
    }
    x=e.pageX;
    y=e.pageY;
    x = x - offset_left;
    y = y - offset_top;

    if(x_array.length == y_array.length) {
      length = x_array.length;
      for (var i = 0; i < length; i++) {
        x_diff=x_array[i]-x;
        y_diff=y_array[i]-y;
        x_diff1=x_diff+width/2;
        distance0=Math.sqrt(x_diff*x_diff+y_diff*y_diff);
        distance1=Math.sqrt(x_diff1*x_diff1+y_diff*y_diff);
        
        if ( (distance0 < rad_array[i])||(distance1 < rad_array[i]))  {
          cirkus(x_array[i],y_array[i],rad_array[i]);
          cirkus(x_array[i]+width/2,y_array[i],rad_array[i]);
          count++;
          if(length == count) {

            clearInterval(timer);
            document.getElementById("timer").innerHTML = 'Congratulations!';
            alert('Congratulations! You\'ve found all the ' + length + ' differences');
          }
          
        }
      }
    }
  }

   function toHHMMSS(seconds) {
    var sec_num = parseInt(seconds); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    var time    = hours+':'+minutes+':'+seconds;
    return time;
  }

   function img() {
    var canvas=document.getElementById("Canvas");
    var img=document.getElementById('image1'); 
    canvas.width = img_width;
    canvas.height =img_height;


    //Refer to the image
    var obCanvas = canvas.getContext('2d');
    obCanvas.drawImage(img,0,0);
    obCanvas.stroke();
  }

  updateTimer = function () {
    total_seconds -= 1;
    document.getElementById("timer_number").innerHTML = toHHMMSS(total_seconds);
    if(total_seconds == 0)
    {
      clearInterval(timer);
      alert('Time Out, click to reload page to restart the game.');
      location.reload();
    } 
  }

  var timer;

  // window.onload = function() {

  // };

  Template.GameCanvasSpotTheDifference.rendered = function(){
  	// Set up the canvas.
    img();
    timer = setInterval(updateTimer, 1000);
  }

  Template.GameCanvasSpotTheDifference.events({
    'click #Canvas': function(evt) {
        position(evt);
    }
  });
}
