/**
 * kidsdailies-google-maps.js
 *
 * Kids Dailies Google Maps JS.
 * 
 * @author Adrian Iacob <adrian@bmcd.co>
 * @author Nitin N <nitin@bmcd.co>
 * @version 0.1
 */

if (Meteor.isClient) {
  var map = null;
  var geocoder = null;
  var markersArray = [];

  /**
   * Map initialisation.
   *
   * @param void
   * @return void
   */
  initialize = function() {
    _emptyOverlays();
    var myOptions = {
      zoom: 2,
      center: new google.maps.LatLng(47.288828765662416, 7.945261001586914),
      disableDefaultUI: true,
      zoomControl: true,
      mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById("google_map_canvas"), myOptions);
  }

  /**
   * Empty overlays array.
   *
   * @param void
   * @return void
   * @access private
   */
  _emptyOverlays = function() {
    if (markersArray) {
      for (i in markersArray) {
        markersArray[i].setMap(null);
      }
      markersArray.length = 0;
    }
  }

  /**
   * Geo-code story address.
   *
   * @param string location
   * @return void
   * @access private
   */
  _findStoryAddress = function(location) {
    var geocoder = new google.maps.Geocoder();
    if (geocoder) {
      geocoder.geocode({ 'address': location, 'componentRestrictions': {'country': location}}, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
            if (results && results[0] && results[0].geometry && results[0].geometry.viewport)
              //map.fitBounds(results[0].geometry.viewport);
              _addMarker(results[0].geometry.location, location);
          }
        }
      });
    }
  }

  /*
   * Add marker to map.
   *
   * @param string location
   * @return void
   * @access private
   */

  _addMarker = function(location, country) {
    marker = new google.maps.Marker({
      position: location,
      draggable: true,
      raiseOnDrag: true,
      map: map
    });

    navigator.geolocation.getCurrentPosition(function(position){
      latitude = position.coords.latitude;
      longitude = position.coords.longitude;

      distance = Math.round(_calculateDistance(location.k, location.A, latitude, longitude));

      var legend = new google.maps.InfoWindow({
        content: "You are " + distance + "km from " + country 
      });

      google.maps.event.addListener(marker, "mouseover", function (e) { legend.open(map, this); });
    });
  }

  /**
   * Calculate distance.
   *
   * @param int lat1
   * @param int lon1
   * @param int lat2
   * @param int lon2
   * @return int
   * @access private
   */
  function _calculateDistance(lat1, lon1, lat2, lon2) {
    var R = 6371; // km
    var dLat = (lat2 - lat1).toRad();
    var dLon = (lon2 - lon1).toRad(); 
    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(lat1.toRad()) * Math.cos(lat2.toRad()) * 
            Math.sin(dLon / 2) * Math.sin(dLon / 2); 
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)); 
    var d = R * c;
    return d;
  }

  Number.prototype.toRad = function() {
    return this * Math.PI / 180;
  }

  Template.MapCanvas.rendered = function() {
    GoogleMaps.init({'sensor': true}, initialize)
      // Load stories
    Stories.find().fetch().forEach(function(story){
      _findStoryAddress(story.location);
    });
  }
}

if (Meteor.isServer) {
  // Populate Mongo with some test data.
  Meteor.startup(function () {
  });
}