/**
 * kidsdailies-text-to-speech.js
 *
 * Kids Dailies Text To Speech JS.
 * 
 * @author Adrian Iacob <adrian@bmcd.co>
 * @author Nitin N <nitin@bmcd.co>
 * @author Ben <ben@bmcd.co
 * @version 0.1
 */

if (Meteor.isClient) {
  var audio
  var count
  var timer
  var markers = {}
  var wordList_widget
  var word_widgets = []
  var ispeech_api_key = '743dc21567e994cb86846dc18222313e'
  var ispeech_api_key_dev = '59e482ac28dd52db23a22aff4ac1d31e'
  var ispeech_tts = 'http://api.ispeech.org/api/rest?format=mp3&action=convert&apikey='
  var ispeech_tts_marker = 'http://api.ispeech.org/api/rest?action=markers&apikey='
  
  function getMakers(msg, callback){
    var url = ispeech_tts_marker + ispeech_api_key + '&voice='+ msg.voice + '&text=' + msg.toSay;    
     $.ajax({
        type: "GET",
        url: url,
        cache: false,
        dataType: "xml",
        success: function(xml) {
            console.log('success')
            markers.text = $(xml).find('text').eq(0).text().trim()
            markers.length = parseInt($(xml).find('length').eq(0).text())
            markers.count = parseInt($(xml).find('words').text())
            markers.voice = $(xml).find('voice').text()
            markers.words = []
            wordList_widget.html("");
            $(xml).find('word').each(function(index){
              var word = {
                start: parseInt($(this).find('start').text()),
                end: parseInt($(this).find('end').text()),
                index: parseInt($(this).find('index').text()),
                length: parseInt($(this).find('length').text()),
                text: $(this).find('text').text()
              }
              markers.words.push(word)
              word_widgets[index] = $("<span>").text(word.text);
              wordList_widget.append(word_widgets[index]);
              if(markers.voice == "usenglishmale" || markers.voice == "usenglishfemale"){
                wordList_widget.append(" ")
              }
            })
            callback()
        }
    });
  }

  // function speak_A(msg){
  //   var url = ispeech_tts + ispeech_api_key_dev + '&voice=' + msg.voice + '&text=' + msg.toSay;
  //   audio = new Audio();    
  //   audio.src = url;
  //   var i = 0
  //   audio.addEventListener("timeupdate", function(){
  //     if(audio.currentTime !=0 && i < word_widgets.length){
  //       wordList_widget.find('span').removeClass('highlight')
  //       word_widgets[i].addClass("highlight");
  //       i++;
  //     }
  //   })
  //   audio.addEventListener("ended", function(){
  //     wordList_widget.find('span').removeClass('highlight')
  //   })
  //   audio.play();  
  // }
  function speak(msg){
    getMakers(msg, function(){
      var url = ispeech_tts + ispeech_api_key_dev + '&voice=' + msg.voice + '&text=' + msg.toSay;
      audio = new Audio();    
      audio.src = url;
      if(markers.voice == 'hkchinesefemale'){
        console.log('hk')
        wordList_widget.html("");
        words = markers.text.split("")
        for(var i = 0; i < words.length; i++){
          word_widgets[i] = $("<span>").text(words[i]);
          wordList_widget.append(word_widgets[i]);
          wordList_widget.append(" ")
        }
        var i = 0
        audio.addEventListener("timeupdate", function(){
          if(audio.currentTime !=0 && i < words.length){
            wordList_widget.find('span').removeClass('highlight')
            word_widgets[i].addClass("highlight");
            i++  
          }
      })
      }else{
        audio.addEventListener("timeupdate", function(){
          var time = audio.currentTime * 1000
          var words =  markers.words
          for(var i = 0; i < markers.count; i++){
            if(time > words[i].start && time < words[i].end){
              wordList_widget.find('span').removeClass('highlight')
              word_widgets[i].addClass("highlight");
              break;  
            }
          }
        })
      }
      audio.addEventListener("ended", function(){
        wordList_widget.find('span').removeClass('highlight')
      })
      audio.play();  
    })
  }
  Template.TextToSpeech.stories = function() {
    return Stories.find()
  }
  Template.TextToSpeech.events({
    "click .btn-speak" : function(){        
        wordList_widget = $("#wordList");
        var toSay = $('#content :selected').text().trim()
        gender = $('input[name=gender]:checked').val()
        lang = $('#lang :selected').val();

        // if(lang == "usenglish"){
        //   var words = toSay.split(" ")
        //   for(var i = 0; i < words.length; i++){
        //     word_widgets[i] = $("<span>").text(words[i]);
        //     wordList_widget.append(word_widgets[i]);
        //     wordList_widget.append(" ")
        //   }
        //   speak_A({toSay: toSay, voice: lang+gender})
        // }else{
        //   var words = toSay.split("")
        //   for(var i = 0; i < words.length; i++){
        //     word_widgets[i] = $("<span>").text(words[i]);
        //     wordList_widget.append(word_widgets[i]);
        //   }
        //   speak_A({toSay: toSay, voice: lang+gender})
        // }
        speak({toSay: toSay, voice: lang+gender})
    }
  })
  Template.TextToSpeech.rendered = function() {
    $("#lang").change(function() {
      if($(this).val() == "hkchinese"){
        $('#male').attr('disabled',true);
        $('#female').attr('checked',true);
      }else{
        $('#male').attr('disabled',false);
        $('#male').attr('checked',true);
      }
    })
    $("#content").change(function() {
      $("#wordList").html("");
      var content = $("#content :selected").text()
      var words = content.split("")
      for(var i = 0; i < words.length; i++)
      {
        $("#wordList").append($('<span>').text(words[i]))
      }
    })
  }
}

if (Meteor.isServer) {
  Meteor.startup(function () {
  });
}
