// var GamesController = RouteController.extend({
//     template: 'games'
// });

Router.map(function () {
    this.route('index', {
        path :  'games',
        template: "GamesTemplate",
        layoutTemplate: "gamesLayout"
    }),
    this.route('crossword', {
        path :  'games/crossword',
        template: "GameCanvasCrossword",
        layoutTemplate: "gamesLayout"
    });
    this.route('image-scramble', {
        path :  'games/image-scramble',
        template: "GameCanvasImageScramble",
        layoutTemplate: "gamesLayout"
    });
    this.route('maze', {
        path :  'games/maze',
        template: "GameCanvasMaze",
        layoutTemplate: "gamesLayout"
    });
    this.route('maze-2', {
        path :  'games/maze-2',
        template: "GameCanvasMaze2",
        layoutTemplate: "gamesLayout"
    });
    this.route('spot-the-difference', {
        path :  'games/spot-the-difference',
        template: "GameCanvasSpotTheDifference",
        layoutTemplate: "gamesLayout"
    });
    this.route('tic-tac-toe', {
        path :  'games/tic-tac-toe',
        template: "GameCanvasTicTacToe",
        layoutTemplate: "gamesLayout"
    });
    this.route('wordsearch', {
        path :  'games/wordsearch',
        template: "GameCanvasWordSearch",
        layoutTemplate: "gamesLayout"
    });
});
