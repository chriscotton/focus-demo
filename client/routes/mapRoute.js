Router.map(function () {
    this.route('google-maps', {
        path :  '/google-maps',
        template: 'MapCanvas',
        layoutTemplate: "mapsLayout"
    });
});
