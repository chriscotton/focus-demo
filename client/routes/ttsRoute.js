Router.map(function () {
    this.route('tts', {
        path :  '/tts',
        template: 'TextToSpeech',
        layoutTemplate: "ttsLayout"
    });
});
