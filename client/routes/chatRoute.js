var ChatController = RouteController.extend({
    template: 'chatTemplate'
});

Router.map(function () {
    this.route('chat', {
        path :  '/chat',
        layoutTemplate: 'chatLayout',
        controller :  ChatController
    });
});
