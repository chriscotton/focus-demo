## Kids Dailies Reader Games App

This is here to give you an overview of The Reader project, with a console tool to ease up some tasks. Clone this repo to get started. 

## How to install and run

Install Meteorite (if not already installed)
```
npm install -g meteorite
```
Start Admin from project root 
```
mrt -p <optional port number>
```
Default port is 3000. To view it 
```
htp://localhost:3000
```
### Packages used

* semantic-ui
* collection2
* less
* jquery
* underscore
* handlebar-helpers
* iron-router
* iron-router-progress
* accounts-password
* accounts-ui
* autoform

## What's in this project

The "insecure" and "autopublish" packages are removed by default. Several other packages are added, which are listed on the bottom. 

### Features

* Comprehensive folder structure
* TDD / BDD with [laika](http://arunoda.github.io/laika/)
* Multi page apps with [iron-router](https://github.com/EventedMind/iron-router)
* A way to load fixtures (as of now no external packages used for that)
* meteor-boilerplate console tool, which helps on creating views, routes and so on (meteor-boilerplate.bat for windows users)
* realtime updates of topstories in english. ( Must have content demo pack if running offsite ) 

### Folder structure

```
client/ 				# Client folder
    compatibility/      # Libraries which create a global variable
    config/             # Configuration files (on the client)
	lib/                # Library files that get executed first
    routes/             # All routes(*)
    startup/            # Javascript files on Meteor.startup()
    stylesheets         # LESS files
    subscriptions/      # Collection subscriptions(*)
    modules/            # Meant for components, such as form and more(*)
	views/			    # Contains all views(*)
	    common/         # General purpose html templates
model/  				# Model files, for each Meteor.Collection(*)
private/                # Private files
public/                 # Public files
server/					# Server folder
    fixtures/           # Meteor.Collection fixtures defined
    lib/                # Server side library folder
    publications/       # Collection publications(*)
    startup/            # On server startup
tests/					# Test files, can be run with laika
meteor-boilerplate		# Command line tool
meteor-boilerplate.bat  # Command line tool for windows

```

(*) = the command line tool creates files in these folders