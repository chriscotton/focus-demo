Maze_2 = new Meteor.Collection2('maze_2');

// Collection2 already does schema checking"
// Add custom permission rules if needed"
Maze_2.allow({
    insert : function () {
        return true;
    },
    update : function () {
        return true;
    },
    remove : function () {
        return true;
    }
});
