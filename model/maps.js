Maps = new Meteor.Collection2('maps');

// Collection2 already does schema checking"
// Add custom permission rules if needed"
Maps.allow({
    insert : function () {
        return true;
    },
    update : function () {
        return true;
    },
    remove : function () {
        return true;
    }
});
