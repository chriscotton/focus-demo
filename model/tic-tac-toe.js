Tic_tac_toe = new Meteor.Collection2('tic-tac-toe');

// Collection2 already does schema checking"
// Add custom permission rules if needed"
Tic_tac_toe.allow({
    insert : function () {
        return true;
    },
    update : function () {
        return true;
    },
    remove : function () {
        return true;
    }
});
