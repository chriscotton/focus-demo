Crossword = new Meteor.Collection2('crossword');

// Collection2 already does schema checking"
// Add custom permission rules if needed"
Crossword.allow({
    insert : function () {
        return true;
    },
    update : function () {
        return true;
    },
    remove : function () {
        return true;
    }
});
