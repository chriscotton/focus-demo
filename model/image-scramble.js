Image_scramble = new Meteor.Collection2('image-scramble');

// Collection2 already does schema checking"
// Add custom permission rules if needed"
Image_scramble.allow({
    insert : function () {
        return true;
    },
    update : function () {
        return true;
    },
    remove : function () {
        return true;
    }
});
