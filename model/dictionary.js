Dictionary = new Meteor.Collection2('dictionary');

// Collection2 already does schema checking"
// Add custom permission rules if needed"
Dictionary.allow({
    insert : function () {
        return true;
    },
    update : function () {
        return true;
    },
    remove : function () {
        return true;
    }
});
