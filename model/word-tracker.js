Word_tracker = new Meteor.Collection2('word-tracker');

// Collection2 already does schema checking"
// Add custom permission rules if needed"
Word_tracker.allow({
    insert : function () {
        return true;
    },
    update : function () {
        return true;
    },
    remove : function () {
        return true;
    }
});
